//
//  RecipeDetailsRouter.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 10/1/20.

import UIKit

class RecipeDetailsRouter: RecipeDetailsWireframeProtocol {
    weak var viewController: UIViewController?

    static func createModule(_ recipe: RecipeModel) -> UIViewController {
        let view = RecipeDetailsViewController(nibName: nil, bundle: nil)
        let interactor = RecipeDetailsInteractor()
        let router = RecipeDetailsRouter()
        let presenter = RecipeDetailsPresenter(interface: view, interactor: interactor, router: router, recipe: recipe)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }
}
