//
//  RecipeDetailsProtocols.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 10/1/20.

import Foundation

// MARK: Wireframe -

protocol RecipeDetailsWireframeProtocol: AnyObject {}

// MARK: Presenter -

protocol RecipeDetailsPresenterProtocol: AnyObject {
    var interactor: RecipeDetailsInteractorInputProtocol? { get set }
    func fetchRecipe()
}

// MARK: Interactor -

protocol RecipeDetailsInteractorOutputProtocol: AnyObject {
    /* Interactor -> Presenter */
}

protocol RecipeDetailsInteractorInputProtocol: AnyObject {
    var presenter: RecipeDetailsInteractorOutputProtocol? { get set }

    /* Presenter -> Interactor */
}

// MARK: View -

protocol RecipeDetailsViewProtocol: AnyObject {
    var presenter: RecipeDetailsPresenterProtocol? { get set }

    /* Presenter -> ViewController */
    func setRecipe(_ recipe: RecipeModel)
}
