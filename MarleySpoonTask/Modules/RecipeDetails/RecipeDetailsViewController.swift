//
//  RecipeDetailsViewController.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 10/1/20.

import UIKit

class RecipeDetailsViewController: UIViewController, RecipeDetailsViewProtocol {
    var presenter: RecipeDetailsPresenterProtocol?

    @IBOutlet private var titleLbl: UILabel!
    @IBOutlet private var recipeDescription: UITextView!
    @IBOutlet private var recipeImg: UIImageView!
    @IBOutlet private var recipeTags: UILabel!
    @IBOutlet private var recipeChef: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.fetchRecipe()
    }

    func setRecipe(_ recipe: RecipeModel) {
        titleLbl.text = recipe.title
        if let url = try? recipe.asset?.url() {
            recipeImg.af.setImage(withURL: url, placeholderImage: UIImage(named: "placholder"))
        }
        recipeDescription.text = recipe.description
        if let chefName = recipe.chef?.name {
            recipeChef.text = NSLocalizedString("Recipe.By.Chef", comment: "") + " " + chefName
        }

        recipeTags.text = recipe.tags?.map { ($0.name ?? "") }.joined(separator: ",")
    }
}
