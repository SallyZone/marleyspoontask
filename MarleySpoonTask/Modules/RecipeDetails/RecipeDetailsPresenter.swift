//
//  RecipeDetailsPresenter.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 10/1/20.

import UIKit

class RecipeDetailsPresenter: RecipeDetailsPresenterProtocol, RecipeDetailsInteractorOutputProtocol {
    private weak var view: RecipeDetailsViewProtocol?
    var interactor: RecipeDetailsInteractorInputProtocol?
    private let router: RecipeDetailsWireframeProtocol
    private let recipe: RecipeModel?
    init(interface: RecipeDetailsViewProtocol, interactor: RecipeDetailsInteractorInputProtocol?, router: RecipeDetailsWireframeProtocol, recipe: RecipeModel) {
        view = interface
        self.interactor = interactor
        self.router = router
        self.recipe = recipe
    }

    func fetchRecipe() {
        if let recipe = recipe {
            view?.setRecipe(recipe)
        }
    }
}
