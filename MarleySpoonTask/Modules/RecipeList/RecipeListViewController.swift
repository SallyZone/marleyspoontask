//
//  RecipeListViewController.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 9/30/20.

import UIKit

class RecipeListViewController: UIViewController, RecipeListViewProtocol {
    @IBOutlet private var loader: UIActivityIndicatorView!
    @IBOutlet private var collectionView: UICollectionView!
    var presenter: RecipeListPresenterProtocol?
    private var recipes: [RecipeModel]?
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle()
        setuplayout()
        presenter?.fetchRecipeList()
    }

    private func setTitle() {
        title = NSLocalizedString("RecipeList.Title", comment: "")
    }

    private func setuplayout() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 200)
        layout.minimumLineSpacing = 2.0
        layout.minimumInteritemSpacing = 2.0
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.register(UINib(nibName: "RecipeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RecipeCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    func setRecipeList(_ recipes: [RecipeModel]) {
        self.recipes = recipes
        loader.isHidden = true
        collectionView.reloadData()
    }
}

extension RecipeListViewController: UICollectionViewDelegate {
    func collectionView(_: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let recipe = recipes?[indexPath.row] {
            presenter?.gotoDetails(recipe)
        }
    }
}

extension RecipeListViewController: UICollectionViewDataSource {
    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        recipes?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecipeCollectionViewCell", for: indexPath)
        (cell as? RecipeCollectionViewCell)?.recipe = recipes?[indexPath.row]
        return cell
    }
}
