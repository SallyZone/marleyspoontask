//
//  RecipeCollectionViewCell.swift
//  MarleySpoonTask
//
//  Created by Sally Ahmed on 9/30/20.
//

import AlamofireImage
import UIKit
class RecipeCollectionViewCell: UICollectionViewCell {
    @IBOutlet private var titleLbl: UILabel!
    @IBOutlet private var imageView: UIImageView!
    var recipe: RecipeModel? {
        didSet {
            self.titleLbl.text = recipe?.title
            if let url = try? recipe?.asset?.url() {
                self.imageView.af.setImage(withURL: url, placeholderImage: UIImage(named: "placholder"))
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
