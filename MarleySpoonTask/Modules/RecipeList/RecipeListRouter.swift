//
//  RecipeListRouter.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 9/30/20.

import UIKit

class RecipeListRouter: RecipeListWireframeProtocol {
    weak var viewController: UIViewController?

    static func createModule() -> UIViewController {
        let view = RecipeListViewController(nibName: nil, bundle: nil)
        let interactor = RecipeListInteractor()
        let router = RecipeListRouter()
        let presenter = RecipeListPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }

    func gotoDetails(_ recipe: RecipeModel) {
        viewController?.navigationController?.pushViewController(RecipeDetailsRouter.createModule(recipe), animated: true)
    }
}
