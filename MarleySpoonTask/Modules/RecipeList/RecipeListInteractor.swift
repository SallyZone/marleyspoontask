//
//  RecipeListInteractor.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 9/30/20.

import Contentful
import UIKit

class RecipeListInteractor: RecipeListInteractorInputProtocol {
    private let spaceId = "kk2bw5ojx476"
    private let environmentId = "master"
    private let accessToken = "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c"
    weak var presenter: RecipeListInteractorOutputProtocol?
    func fetchRecipeList() {
        let client = Client(spaceId: spaceId,
                            environmentId: environmentId,
                            accessToken: accessToken,
                            contentTypeClasses: [RecipeModel.self, ChefModel.self, TagModel.self])
        print(client)
        // let query = Query.where(contentTypeId: "recipe")
        let query = QueryOn<RecipeModel>.where(contentTypeId: "recipe")

        client.fetchArray(of: RecipeModel.self, matching: query) { [weak self] result in
            switch result {
            case let .success(entriesArrayResponse):
                DispatchQueue.main.async { [weak self] in
                    self?.presenter?.setRecipeList(entriesArrayResponse.items)
                }
            case let .failure(error):
                print("Oh no something went wrong: \(error)")
            }
        }
    }
}
