//
//  RecipeListProtocols.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 9/30/20.

import Foundation

// MARK: Wireframe -

protocol RecipeListWireframeProtocol: AnyObject {
    func gotoDetails(_ recipe: RecipeModel)
}

// MARK: Presenter -

protocol RecipeListPresenterProtocol: AnyObject {
    var interactor: RecipeListInteractorInputProtocol? { get set }
    func fetchRecipeList()
    func gotoDetails(_ recipe: RecipeModel)
}

// MARK: Interactor -

protocol RecipeListInteractorOutputProtocol: AnyObject {
    /* Interactor -> Presenter */
    func setRecipeList(_ recipes: [RecipeModel])
}

protocol RecipeListInteractorInputProtocol: AnyObject {
    var presenter: RecipeListInteractorOutputProtocol? { get set }
    /* Presenter -> Interactor */
    func fetchRecipeList()
}

// MARK: View -

protocol RecipeListViewProtocol: AnyObject {
    var presenter: RecipeListPresenterProtocol? { get set }
    func setRecipeList(_ recipes: [RecipeModel])

    /* Presenter -> ViewController */
}
