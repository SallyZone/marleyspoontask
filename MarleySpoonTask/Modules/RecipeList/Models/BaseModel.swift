//
//  BaseModel.swift
//  MarleySpoonTask
//
//  Created by Sally Ahmed on 9/30/20.
//

import Foundation
class BaseModel {
    var id: String
    var updatedAt: Date?
    var createdAt: Date?
    var localeCode: String?

    required init(from decoder: Decoder) throws {
        let sys = try decoder.sys()

        id = sys.id
        localeCode = sys.locale
        updatedAt = sys.updatedAt
        createdAt = sys.createdAt
    }
}
