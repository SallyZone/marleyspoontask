//
//  RecipeModel.swift
//  MarleySpoonTask
//
//  Created by Sally Ahmed on 9/30/20.
//

import Contentful
import Foundation

final class RecipeModel: BaseModel, EntryDecodable, FieldKeysQueryable {
    static var contentTypeId = "recipe"

    var title: String?
    var description: String?
    var tags: [TagModel]?
    var chef: ChefModel?

    var asset: Asset?
    var thumb: UIImage?

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)

        let fields = try decoder.contentfulFieldsContainer(keyedBy: RecipeModel.FieldKeys.self)

        title = try fields.decode(String.self, forKey: .title)
        description = try fields.decode(String.self, forKey: .description)

        try fields.resolveLinksArray(forKey: .tags, decoder: decoder) { tags in
            self.tags = tags as? [TagModel]
        }

        try fields.resolveLink(forKey: .chef, decoder: decoder) { chef in
            self.chef = chef as? ChefModel
        }

        try fields.resolveLink(forKey: .photo, decoder: decoder) { image in
            self.asset = image as? Asset
        }
    }

    enum FieldKeys: String, CodingKey {
        case title, description, tags, chef, photo
    }
}
