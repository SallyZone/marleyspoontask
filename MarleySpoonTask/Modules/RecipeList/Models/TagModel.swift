//
//  TagModel.swift
//  MarleySpoonTask
//
//  Created by Sally Ahmed on 9/30/20.
//

import Contentful
import Foundation
final class TagModel: BaseModel, EntryDecodable, FieldKeysQueryable {
    static var contentTypeId = "tag"

    var name: String?

    enum FieldKeys: String, CodingKey {
        case name
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)

        let fields = try decoder.contentfulFieldsContainer(keyedBy: FieldKeys.self)
        name = try fields.decode(String.self, forKey: .name)
    }
}
