//
//  RecipeListPresenter.swift
//  MarleySpoonTask
//
//  Created Sally Ahmed on 9/30/20.

import UIKit

class RecipeListPresenter: RecipeListPresenterProtocol, RecipeListInteractorOutputProtocol {
    private weak var view: RecipeListViewProtocol?
    var interactor: RecipeListInteractorInputProtocol?
    private let router: RecipeListWireframeProtocol

    init(interface: RecipeListViewProtocol, interactor: RecipeListInteractorInputProtocol?, router: RecipeListWireframeProtocol) {
        view = interface
        self.interactor = interactor
        self.router = router
    }

    func fetchRecipeList() {
        interactor?.fetchRecipeList()
    }

    func setRecipeList(_ recipes: [RecipeModel]) {
        view?.setRecipeList(recipes)
    }

    func gotoDetails(_ recipe: RecipeModel) {
        router.gotoDetails(recipe)
    }
}
