# MarleySpoonTask

## Before you launch the app
Run `pod install` first to install dependencies.

## Things to know
-  I used VIPER architecture for modules
- Used dependencies 
    - Contentful for API service
    - AlamofireImage for lazy image download
    - swiftformat for formating swift style 
    - swiftlint for apply linting rules 


